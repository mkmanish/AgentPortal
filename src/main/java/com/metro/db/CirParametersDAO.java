package com.metro.db;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.metro.core.CirParameters;
import org.json.simple.JSONObject;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

public interface CirParametersDAO {

    @SqlQuery("with cir_data as ( select *, rank() over (partition by loan_id order by id desc) from" +
            " epl_credit_loan_cir_parameters_new order by loan_id, rank ) select  ca.marketplace_customer_id," +
            " cap.company_name, cap.id as \"Application Id\", loan.id as \"Loan Id\","+
            " cir.cir_parameters_json->>'api_name' as \"Api Name\"," +
            " cir.cir_parameters_json->>'credit_score' as \"Credit Score\"," +
            " cir.cir_parameters_json->'cir_parameters'->>'credit_score' as \"Credit score New\"," +
            " cir.cir_parameters_json->>'loanApproved' as \"Loan Approved\"," +
            " cir.cir_parameters_json->>'system_limit' as \"System Limit\"," +
            " cir.cir_parameters_json->>'tenure' as \"Tenure in Months\"," +
            " cir.cir_parameters_json->'sales_parameters'->>'tenure' as \"Tenure in Months2\"" +
            " from epl_credit_application cap join epl_credit_applicant ca on ca.id = cap.applicant_id" +
            " join epl_credit_loan_new loan on loan.application_id = cap.id left join cir_data cir on" +
            " cir.loan_id = loan.id where cir.rank = 1 and cap.marketplace_id = :marketplace_id and" +
            " ca.marketplace_customer_id = :marketplace_customer_id")
    @RegisterMapper(CirParametersMapper.class)
    public JSONObject uploadDocuments(@Bind("marketplace_id") long marketplace_id,
                                      @Bind("marketplace_customer_id") String marketplace_customer_id);
}
