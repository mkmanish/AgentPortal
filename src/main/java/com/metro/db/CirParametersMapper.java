package com.metro.db;

import com.metro.core.CirParameters;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;
import org.json.simple.JSONObject;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CirParametersMapper implements ResultSetMapper<JSONObject> {

    @Override
    public JSONObject map(int i, ResultSet r, StatementContext statementContext) throws SQLException {


        /**
         * Taking the data from the resultset and assigning them to the corresponding variables defined in the CirParameters
         */

        String marketplace_customer_id = r.getString("marketplace_customer_id");
        String company_name = r.getString("company_name");
        long application_id = r.getLong("Application Id");
        long loan_id = r.getLong("Loan Id");
        String api_name = r.getString("Api Name");
        long credit_score = r.getLong("Credit Score");
        long credit_score_new = r.getLong("Credit score New");
        long loanApproved = r.getLong("Loan Approved");
        long system_limit = r.getLong("System Limit");
        double tenure = r.getDouble("Tenure in Months");
        double tenure_new = r.getDouble("Tenure in Months2");

        int decision = 1; //Need bank statement
        JSONObject obj = new JSONObject();

        /**
         * Implementing the required conditions for deciding whether Bank Statement is required or not
         * based on 3 parameters extracted from the resultset namely:
         * 1.credit_score
         * 2.tenure
         * 3.system_limit
         */

        if (credit_score >= 300 || credit_score_new >= 300) {

            if ((tenure < 6 || tenure_new < 6) && system_limit < 50000) {
                decision = 0;                                                       //case : A
            } else if (tenure >= 6 || tenure_new >= 6) {
                                                                                    //case : B
                if (system_limit >= 50000) {
                    decision = 1;
                } else {
                    decision = 0;
                }
            }
        } else if (tenure >= 6 || tenure_new >= 6) {
            decision = 1;                                                               //case : D
        } else {
            decision = 2;    //No credit Allowed                                         //case : C
        }

        if(decision==0){
            obj.put("Document Required" , "None");
        }

        if(decision==1){
            obj.put("Document Required" , "Bank Statement");
        }
        else if(decision==2){
            obj.put("Issue" , "Can't Provide Credit");
        }

        return obj;
    }
}
