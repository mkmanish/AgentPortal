package com.metro.resources;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.metro.core.CirParameters;
import com.metro.db.CirParametersDAO;
import org.json.simple.JSONObject;
import org.skife.jdbi.v2.DBI;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.awt.*;


@Path("/cirParameters")
public class Resource {


    CirParametersDAO cirDao;
    public Resource(DBI dbi) {

        cirDao = dbi.onDemand(CirParametersDAO.class);
    }


    /**
     * API call for fetching data from the database
     * @param marketplace_id
     * @param marketplace_customer_id
     * @return
     */

    @Path("/documents/{marketplace_id}/{marketplace_customer_id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response uploadDocuments(@PathParam("marketplace_id") long marketplace_id,
                                    @PathParam("marketplace_customer_id") String marketplace_customer_id) {

        JSONObject documents = cirDao.uploadDocuments(marketplace_id,marketplace_customer_id);
        return Response.status(Response.Status.OK).entity(documents).build();
    }
}
