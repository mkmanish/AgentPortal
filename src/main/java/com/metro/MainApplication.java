package com.metro;

import com.metro.resources.Resource;
import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.jdbi.DBIFactory;
import org.skife.jdbi.v2.DBI;

public class MainApplication extends Application<MainConfiguration> {

    public static void main(final String[] args) throws Exception {
        new MainApplication().run(args);
        System.out.print("Hey there!");
    }

    @Override
    public String getName() {
        return "Main";
    }

    @Override
    public void initialize(final Bootstrap<MainConfiguration> bootstrap) {
        // TODO: application initialization

        bootstrap.addBundle(new MigrationsBundle<MainConfiguration>() {
            @Override
            public DataSourceFactory getDataSourceFactory(MainConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        });
    }

    @Override
    public void run(final MainConfiguration configuration, final Environment environment) {
        // TODO: implement application

        /**
         * Creating a DBIFactory instance for establishing connection to the postgresql database
         */

        DBIFactory dbiFactory = new DBIFactory();
        DBI dbi = dbiFactory.build(environment,configuration.getDataSourceFactory(),"postgresql");

        environment.jersey().register(new Resource(dbi));
    }

}
