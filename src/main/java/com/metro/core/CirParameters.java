package com.metro.core;


/**
 * POJO corresponding to the dataset required for decision making : whether bank statement is required or not
 */

public class CirParameters {

        private String marketplace_customer_id;
        private String company_name;
        private long application_id;
        private long loan_id;
        private String api_name;
        private long credit_score;
        private long credit_score_new;
        private long loanApproved;
        private long system_limit;
        private double tenure;
        private double tenure_new;

        //Constructor to be used by mapper

    public CirParameters(String marketplace_customer_id, String company_name, long application_id, long loan_id,
                         String api_name, long credit_score, long credit_score_new, long loanApproved, long system_limit,
                         double tenure, double tenure_new) {
        this.application_id = application_id;
        this.company_name = company_name;
        this.marketplace_customer_id = marketplace_customer_id;
        this.loan_id = loan_id;
        this.api_name = api_name;
        this.credit_score = credit_score;
        this.credit_score_new = credit_score_new;
        this.loanApproved = loanApproved;
        this.system_limit = system_limit;
        this.tenure = tenure;
        this.tenure_new = tenure_new;
    }

    public long getApplication_id() {
        return application_id;
    }

    public void setApplication_id(long application_id) {
        this.application_id = application_id;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getMarketplace_customer_id() {
        return marketplace_customer_id;
    }

    public void setMarketplace_customer_id(String marketplace_customer_id) {
        this.marketplace_customer_id = marketplace_customer_id;
    }

    public long getLoan_id() {
        return loan_id;
    }

    public void setLoan_id(long loan_id) {
        this.loan_id = loan_id;
    }

    public String getApi_name() {
        return api_name;
    }

    public void setApi_name(String api_name) {
        this.api_name = api_name;
    }

    public long getCredit_score() {
        return credit_score;
    }

    public void setCredit_score(long credit_score) {
        this.credit_score = credit_score;
    }

    public long getCredit_score_new() {
        return credit_score_new;
    }

    public void setCredit_score_new(long credit_score_new) {
        this.credit_score_new = credit_score_new;
    }

    public long getLoanApproved() {
        return loanApproved;
    }

    public void setLoanApproved(long loanApproved) {
        this.loanApproved = loanApproved;
    }

    public long getSystem_limit() {
        return system_limit;
    }

    public void setSystem_limit(long system_limit) {
        this.system_limit = system_limit;
    }

    public double getTenure() {
        return tenure;
    }

    public void setTenure(double tenure) {
        this.tenure = tenure;
    }

    public double getTenure_new() {
        return tenure_new;
    }

    public void setTenure_new(double tenure_new) {
        this.tenure_new = tenure_new;
    }
}
